module gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator

go 1.13

require (
	github.com/go-logr/logr v0.1.0
	github.com/imdario/mergo v0.3.9
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v2 v2.3.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	gotest.tools v2.2.0+incompatible
	k8s.io/api v0.18.6
	k8s.io/apimachinery v0.18.6
	k8s.io/client-go v0.18.6
	sigs.k8s.io/controller-runtime v0.6.3
	sigs.k8s.io/controller-tools v0.3.0 // indirect
	sigs.k8s.io/yaml v1.2.0 // indirect
)
