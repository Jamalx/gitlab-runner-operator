package runner

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	gitlabutils "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/utils"
	"gopkg.in/yaml.v2"
)

const (
	// GitLabRunnerImage has name of runner
	GitLabRunnerImage string = "gitlab-runner"

	// GitLabRunnerHelperImage has name of runner helper
	GitLabRunnerHelperImage string = "gitlab-runner-helper"

	// releaseFile is the location where metadata about releases is kept. Look at hack/assets
	releaseFile = "release.yaml"
)

type imageResolver interface {
	HelperImage() string
	RunnerImage() string
}

type releaseDirImageResolver struct {
}

func (r *releaseDirImageResolver) getImageURL(image string) string {
	release := getRelease()

	var targetContainer Image
	for _, container := range release.Images {
		if container.Name == image {
			targetContainer = container
		}
	}

	return targetContainer.Image()
}

func (r *releaseDirImageResolver) HelperImage() string {
	return r.getImageURL(GitLabRunnerHelperImage)
}

func (r *releaseDirImageResolver) RunnerImage() string {
	return r.getImageURL(GitLabRunnerImage)
}

// Image represents a
// single microservice image
type Image struct {
	Name      string `yaml:"name"`
	Upstream  string `yaml:"upstream"`
	Certified string `yaml:"certified"`
}

// Image function auto-selects image to deploy
func (img Image) Image() string {
	if gitlabutils.IsOpenshift() &&
		img.Certified != "" {
		return img.Certified
	}

	return img.Upstream
}

// Release defines a GitLab release
type Release struct {
	Version string  `json:"version" yaml:"version"`
	Images  []Image `json:"images" yaml:"images"`
}

//getRelease reads the release file and returns a release object
func getRelease() *Release {
	dir, err := getReleaseDir()
	if err != nil {
		fmt.Printf("error getting releases directory; %v\n", err)
	}

	releaseFile, err := ioutil.ReadFile(filepath.Join(dir, releaseFile))
	if err != nil {
		return nil
	}

	release := &Release{}
	err = yaml.Unmarshal(releaseFile, release)
	if err != nil {
		return nil
	}

	return release
}

func getReleaseDir() (string, error) {
	_, err := os.Stat("/run/secrets/kubernetes.io/serviceaccount")
	if err == nil {
		return "/", nil
	}

	wd, err := os.Getwd()
	if err != nil {
		return "", err
	}

	if strings.HasSuffix(wd, "gitlab-runner-operator") {
		return filepath.Join(wd, "hack/assets"), nil
	}

	// check if path is project dir is in path but not last dir
	var basePath string
	if strings.Contains(wd, "gitlab-runner-operator") {
		dirs := strings.SplitAfter(wd, "gitlab-runner-operator")
		basePath = dirs[0]
	}

	return filepath.Join(basePath, "hack/assets"), nil
}
